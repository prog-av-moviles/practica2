import 'package:flutter/cupertino.dart';
import 'package:practica2_mov2021/src/models/creditDAO.dart';

class CardCredits extends StatelessWidget {
  const CardCredits({Key key, @required this.credit}) : super(key: key);

  final CreditDAO credit;

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
        child: new Column(children: <Widget>[
          Container(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(200.0),
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      Container(
                        width: 100,
                        height: 100,
                        decoration: new BoxDecoration(shape: BoxShape.circle),
                        child: FadeInImage(
                          placeholder:
                              AssetImage('assets/activity_indicator.gif'),
                          image: (credit.profile_path != null)
                              ? NetworkImage(
                                  'https://image.tmdb.org/t/p/w600_and_h600_bestv2${credit.profile_path}')
                              : AssetImage('assets/activity_indicator.gif'),
                          fadeInDuration: Duration(milliseconds: 100),
                        ),
                      ),
                    ],
                  ))),
          Text(
            credit.name,
            style: new TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
          ),
        ]));
  }
}
