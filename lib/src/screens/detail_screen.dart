import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:practica2_mov2021/src/databases/database_helper.dart';
import 'package:practica2_mov2021/src/models/creditDAO.dart';
import 'package:practica2_mov2021/src/models/popularDAO.dart';
import 'package:practica2_mov2021/src/network/api_popular.dart';
import 'package:practica2_mov2021/src/views/card_credits.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class DetailScreen extends StatefulWidget {
  DetailScreen({Key key}) : super(key: key);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  DatabaseHelper _database = DatabaseHelper();
  ApiPopular apiPopular = ApiPopular();
  String claveVideo;
  bool esFavorito;

  @override
  Widget build(BuildContext context) {
    @override
    final movie =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;

    FutureBuilder iconFav = FutureBuilder(
      future: _database.isFavorite(movie['id']),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          esFavorito = false;
          return new Icon(Icons.favorite_border);
        } else if (snapshot.connectionState == ConnectionState.done) {
          esFavorito = snapshot.data;
          return new Icon(
              esFavorito ? Icons.favorite : Icons.favorite_border_outlined);
        } else {
          esFavorito = false;
          return new Icon(Icons.favorite_border);
        }
      },
    );

    FutureBuilder repback = FutureBuilder(
        future: apiPopular.getVideo(movie['id']),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasError) {
            return Container(
                width: MediaQuery.of(context).size.width,
                child: FadeInImage(
                  placeholder: AssetImage('assets/01.jpg'),
                  image: NetworkImage(
                      'https://image.tmdb.org/t/p/w200/${movie['posterpath']}'),
                  fadeInDuration: Duration(milliseconds: 100),
                ));
          } else if (snapshot.connectionState == ConnectionState.done) {
            return Container(
              width: MediaQuery.of(context).size.width,
              child: (snapshot.data != null)
                  ? YoutubePlayer(
                      controller: YoutubePlayerController(
                        initialVideoId: snapshot.data,
                        flags: YoutubePlayerFlags(
                          autoPlay: false,
                          mute: false,
                          disableDragSeek: false,
                          loop: false,
                          isLive: false,
                          forceHD: false,
                        ),
                      ),
                      liveUIColor: Colors.amber,
                    )
                  : Container(
                      width: MediaQuery.of(context).size.width,
                      child: FadeInImage(
                        placeholder: AssetImage('assets/01.jpg'),
                        image: NetworkImage(
                            'https://image.tmdb.org/t/p/w500/${movie['backdropPath']}'),
                        fadeInDuration: Duration(milliseconds: 100),
                      )),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });

    Widget _listDetailMovie(List<CreditDAO> credits) {
      return ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            CreditDAO credit = credits[index];
            return CardCredits(credit: credit);
          },
          itemCount: credits.length);
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(movie['title']),
          actions: <Widget>[
            IconButton(
              icon: iconFav,
              onPressed: () async {
                if (esFavorito) {
                  await _database
                      .delete("tbl_favorites", movie['id'])
                      .then((data) {
                    setState(() {
                      iconFav = iconFav;
                    });
                    print(data);
                  });
                } else {
                  var peli = {
                    'id': movie['id'],
                    'title': movie['title'],
                    'overview': movie['overview'],
                    'poster_path': movie['posterpath'],
                    'backdrop_path': movie['backdropPath'],
                    'vote_average': movie['voteAverage']
                  };
                  await _database.insert("tbl_favorites", peli).then((data) {
                    setState(() {
                      iconFav = iconFav;
                    });
                    print(data);
                  });
                }

                esFavorito = !esFavorito;
              },
            ),
          ],
        ),
        body: FutureBuilder(
          future: apiPopular.getCredits(movie['id']),
          builder:
              (BuildContext context, AsyncSnapshot<List<CreditDAO>> snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text("Error"));
            } else if (snapshot.connectionState == ConnectionState.done) {
              return new Column(
                children: <Widget>[
                  repback,
                  Card(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(10),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.blue[400],
                            ),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RatingBar.builder(
                                    initialRating: (movie['voteAverage'] / 2),
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding:
                                        EdgeInsets.symmetric(horizontal: 4.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                  Text(movie['voteAverage'].toString(),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      )),
                                ])),
                        ListTile(
                          leading: Icon(Icons.movie),
                          title: Text(movie['title']),
                          subtitle: Text(movie['overview']),
                        ),
                      ],
                    ),
                  ),
                  Expanded(child: _listDetailMovie(snapshot.data)),
                ],
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ));
  }
}
