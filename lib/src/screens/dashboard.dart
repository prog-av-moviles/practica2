import 'dart:io';

import 'package:flutter/material.dart';
import 'package:practica2_mov2021/src/databases/database_helper.dart';
import 'package:practica2_mov2021/src/models/userDAO.dart';
import 'package:practica2_mov2021/src/utils/configuration.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DatabaseHelper _database = DatabaseHelper();

    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      drawer: FutureBuilder(
          future: _database.getUser('16031134@itcelaya.edu.mx'),
          builder: (BuildContext context, AsyncSnapshot<UserDAO> snapshot) {
            //child:
            return Drawer(
              child: ListView(
                children: [
                  UserAccountsDrawerHeader(
                    decoration: BoxDecoration(color: Configuration.colorHeader),
                    accountName: snapshot.data != null
                        ? Text(snapshot.data.nombre)
                        : Text("Usuario invitado"),
                    accountEmail: snapshot.data != null
                        ? Text(snapshot.data.email)
                        : Text("anonimo@gmail.com"),
                    currentAccountPicture: snapshot.data.foto != null
                        ? ClipOval(
                            child: Image.file(
                              File(snapshot.data.foto),
                              fit: BoxFit.cover,
                            ),
                          )
                        : CircleAvatar(
                            backgroundImage: NetworkImage(
                                'https://lh5.googleusercontent.com/-6ATGzx4A5uk/AAAAAAAAAAI/AAAAAAAAAAA/AKxrwcaiSL7HaFoNtgvDVLRTyZlqkgtuRQ/mo/photo.jpg'),
                          ),
                    onDetailsPressed: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/profile',
                          arguments: {'email': snapshot.data.email});
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.trending_up,
                      color: Configuration.colorIcons,
                    ),
                    title: Text('Trending'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/popular');
                    },
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Configuration.colorIcons,
                    ),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.search,
                      color: Configuration.colorIcons,
                    ),
                    title: Text('Search'),
                    onTap: () {
                      Navigator.pop(context);
                    },
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Configuration.colorIcons,
                    ),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.favorite,
                      color: Configuration.colorIcons,
                    ),
                    title: Text('Favorites'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/favorite');
                    },
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Configuration.colorIcons,
                    ),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.contact_page,
                      color: Configuration.colorIcons,
                    ),
                    title: Text('Contact Us'),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, '/contact');
                    },
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Configuration.colorIcons,
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
