import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:practica2_mov2021/src/databases/database_helper.dart';
import 'package:practica2_mov2021/src/models/popularDAO.dart';
import 'package:practica2_mov2021/src/views/card_popular.dart';

class FavoriteScreen extends StatefulWidget {
  FavoriteScreen({Key key}) : super(key: key);

  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  DatabaseHelper _database;

  @override
  void initState() {
    super.initState();
    _database = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorite Movies"),
      ),
      body: FutureBuilder(
        future: _database.getFavorites(),
        builder:
            (BuildContext context, AsyncSnapshot<List<PopularDAO>> snapshot) {
          if (snapshot.hasError) {
            return Center(child: Text("Has error in this request"));
          } else if (snapshot.connectionState == ConnectionState.done) {
            return (snapshot.data.length > 0)
                ? _listPopularMovies(snapshot.data)
                : Center(child: Text("You don't have any favorite movie"));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }

  Widget _listPopularMovies(List<PopularDAO> movies) {
    return ListView.builder(
      itemBuilder: (context, index) {
        PopularDAO popular = movies[index];
        return CardPopular(
          popular: popular,
        );
      },
      itemCount: movies.length,
    );
  }
}
