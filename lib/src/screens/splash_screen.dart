import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:practica2_mov2021/src/screens/login.dart';
import 'package:splashscreen/splashscreen.dart';

class SplashScreenApp extends StatefulWidget {
  SplashScreenApp({Key key}) : super(key: key);

  @override
  _SplashScreenAppState createState() => _SplashScreenAppState();
}

class _SplashScreenAppState extends State<SplashScreenApp> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 10,
      navigateAfterSeconds: Login(),
      title: Text('Bienvenido'),
      image: Image.asset(
        'assets/popcorn.png',
        width: 500,
        height: 500,
      ),
      gradientBackground: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        //colors: [Colors.purple[300], Colors.pink[200]],
        colors: [Colors.red[900], Colors.yellow[700]],
      ),
      loaderColor: Colors.white70,
      loadingText: Text('Iniciando...'),
    );
  }
}
