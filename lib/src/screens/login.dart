import 'package:flutter/material.dart';
import 'package:practica2_mov2021/src/screens/dashboard.dart';

import '../databases/database_helper.dart';
import '../models/userDAO.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  DatabaseHelper _database;

  @override
  void initState() {
    super.initState();
    _database = DatabaseHelper();
  }

  final disney = Image.asset(
    'assets/popcorn.png',
    width: 250,
    height: 120,
  );

  @override
  Widget build(BuildContext context) {
    var emailController = TextEditingController();
    var pwdController = TextEditingController();

    final txtEmail = TextFormField(
      controller: emailController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          hintText: 'Introduce email',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5)),
    );

    final txtPwd = TextFormField(
      controller: pwdController,
      keyboardType: TextInputType.text,
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'Introduce password',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5)),
    );

    final loginButton = RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Text(
          'Validar usuario',
          style: TextStyle(color: Colors.white),
        ),
        color: Colors.blue[300],
        onPressed: () async {
          if (await _database.getUser("16031134@itcelaya.edu.mx") == null) {
            UserDAO objUSER = UserDAO(
                nombre: "Raquel Velasco Franco",
                email: "16031134@itcelaya.edu.mx",
                telefono: "4111050206",
                foto: null);
            await _database.insert("tbl_perfil", objUSER.toJSON());
            print("Cree el usuario");
          } else {
            print("YA existe el usuario");
          }
          //Validar usuario mediante API
          Navigator.pushNamed(context, '/dashboard');
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Dashboard()));
        });

    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/cine.jpg'), fit: BoxFit.fill)),
        ),
        Card(
            color: Colors.white,
            margin: EdgeInsets.all(30.0),
            elevation: 10.0,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView(
                shrinkWrap: true,
                children: [
                  txtEmail,
                  SizedBox(
                    height: 10,
                  ),
                  txtPwd,
                  SizedBox(
                    height: 20,
                  ),
                  loginButton
                ],
              ),
            )),
        Positioned(
          child: disney,
          bottom: 300,
        )
      ],
    );
  }
}
