import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_boom_menu/flutter_boom_menu.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ConctactScreen extends StatefulWidget {
  ConctactScreen({Key key}) : super(key: key);

  @override
  _ConctactScreenState createState() => _ConctactScreenState();
}

class _ConctactScreenState extends State<ConctactScreen> {
  @override
  Widget build(BuildContext context) {
    Completer<GoogleMapController> _controller = Completer();
    CameraPosition _myPosition =
        CameraPosition(target: LatLng(20.5397285, -100.8139141), zoom: 25.0);
    return Scaffold(
      body: GoogleMap(
        mapType: MapType.hybrid,
        initialCameraPosition: _myPosition,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: _BuildBoomMenu(),
    );
  }

  _BuildBoomMenu() {
    return BoomMenu(
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 20),
      overlayColor: Colors.black,
      overlayOpacity: 0.7,
      children: [
        MenuItem(
            child: Icon(MdiIcons.gmail),
            title: 'Email',
            titleColor: Colors.green[850],
            subTitleColor: Colors.grey[850],
            subtitle: '16031134@itcelaya.edu.mx',
            backgroundColor: Colors.blue[50],
            onTap: () => _sendEmail()),
        MenuItem(
            child: Icon(MdiIcons.phone),
            title: 'Telefono',
            titleColor: Colors.green[850],
            subTitleColor: Colors.grey[850],
            subtitle: '4111050206',
            backgroundColor: Colors.blue[50],
            onTap: () => _callPhone()),
        MenuItem(
            child: Icon(MdiIcons.message),
            title: 'Mensaje',
            titleColor: Colors.green[850],
            subTitleColor: Colors.grey[850],
            subtitle: 'tel:4111050206',
            backgroundColor: Colors.blue[50],
            onTap: () => _sendMessage())
      ],
    );
  }

  _sendEmail() async {
    final Uri params = Uri(
        scheme: 'mailto',
        path: '16031134@itcelaya.edu.mx',
        query: 'subject=Enviando email&body=Hola desde mi app');

    var email = params.toString();
    if (await canLaunch(email)) {
      await launch(email);
    }
  }

  _callPhone() async {
    const tel = 'tel:4111050206';
    if (await canLaunch(tel)) {
      await launch(tel);
    }
  }

  _sendMessage() async {
    const tel = 'sms:4111050206';
    if (await canLaunch(tel)) {
      await launch(tel);
    }
  }
}
