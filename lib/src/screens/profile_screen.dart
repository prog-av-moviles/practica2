import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:practica2_mov2021/src/databases/database_helper.dart';
import 'package:practica2_mov2021/src/models/userDAO.dart';
import 'package:practica2_mov2021/src/screens/dashboard.dart';
import 'package:practica2_mov2021/src/utils/configuration.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  TextEditingController nombreController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController telefonoController = TextEditingController();

  DatabaseHelper _database;

  final picker = ImagePicker();
  String imagePath = "";

  @override
  void initState() {
    super.initState();
    _database = DatabaseHelper();
  }

  @override
  Widget build(BuildContext context) {
    final perfilUser =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final imgPerfil = imagePath == ""
        ? CircleAvatar(
            backgroundImage: NetworkImage(
                'https://lh5.googleusercontent.com/-6ATGzx4A5uk/AAAAAAAAAAI/AAAAAAAAAAA/AKxrwcaiSL7HaFoNtgvDVLRTyZlqkgtuRQ/mo/photo.jpg'),
            radius: 100,
          )
        : ClipOval(
            child: Image.file(
              File(imagePath),
              fit: BoxFit.cover,
            ),
          );

    final txtName = TextFormField(
      controller: nombreController,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
          hintText: 'Nombre',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)),
    );

    final txtEmail = TextFormField(
      controller: emailController,
      enabled: true,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          hintText: 'Email',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)),
    );

    final txtPhone = TextFormField(
      controller: telefonoController,
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
          hintText: 'Teléfono',
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          contentPadding: EdgeInsets.fromLTRB(20, 5, 20, 5)),
    );

    final btnNewImage = RawMaterialButton(
      child: Icon(Icons.image_search),
      onPressed: () async {
        final file = await picker.getImage(source: ImageSource.camera);
        imagePath = file.path;
        setState(() {});
        //setState(() {});
      },
      elevation: 2.0,
      fillColor: Configuration.colorIcons,
      padding: EdgeInsets.all(10.0),
      shape: CircleBorder(),
    );

    final btnSave = RaisedButton(
      color: Configuration.colorIcons,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(
        'Guardar cambios',
        style: TextStyle(color: Colors.white),
      ),
      onPressed: () async {
        var usuario = await _database.getUser(perfilUser["email"]);
        UserDAO objUSER = UserDAO(
            id: usuario.id,
            nombre: nombreController.text,
            email: emailController.text,
            telefono: telefonoController.text,
            foto: imagePath);
        print(objUSER.toJSON());
        _database.update("tbl_perfil", objUSER.toJSON());
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext context) => Dashboard()),
            ModalRoute.withName('/dashboard'));
      },
    );

    return Container(
      child: Scaffold(
          appBar: AppBar(
            title: Text('Profile'),
            backgroundColor: Configuration.colorHeader,
          ),
          body: Stack(
            children: <Widget>[
              Card(
                color: Colors.white10,
                margin: EdgeInsets.all(0.0),
                elevation: 0.0,
                child: Padding(
                  padding: EdgeInsets.all(10),
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      imgPerfil,
                      SizedBox(
                        height: 10,
                      ),
                      btnNewImage,
                      SizedBox(
                        height: 10,
                      ),
                      txtName,
                      SizedBox(
                        height: 10,
                      ),
                      txtEmail,
                      SizedBox(
                        height: 10,
                      ),
                      txtPhone,
                      SizedBox(
                        height: 10,
                      ),
                      btnSave
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
