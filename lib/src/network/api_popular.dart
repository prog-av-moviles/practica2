import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'package:practica2_mov2021/src/models/creditDAO.dart';
import 'package:practica2_mov2021/src/models/popularDAO.dart';
import 'package:practica2_mov2021/src/models/videoDAO.dart';

class ApiPopular {
  final String URL_POPULAR =
      "https://api.themoviedb.org/3/movie/popular?api_key=2fff0af21d6ba317bfd7f575d89c66e6&language=en-US&page=1";
  Client http = Client();

  Future<List<PopularDAO>> getAllPopular() async {
    final response = await http.get(URL_POPULAR);
    if (response.statusCode == 200) {
      var popular = jsonDecode(response.body)['results'] as List;
      List<PopularDAO> listPopular =
          popular.map((movie) => PopularDAO.fromJSON(movie)).toList();
      print(listPopular.length);
      return listPopular;
    } else {
      return null;
    }
  }

  Future<List<CreditDAO>> getCredits(int idMovie) async {
    final response = await http.get(
        "https://api.themoviedb.org/3/movie/${idMovie}/credits?api_key=2fff0af21d6ba317bfd7f575d89c66e6&language=en-US");
    print(response.statusCode);
    if (response.statusCode == 200) {
      var movies = jsonDecode(response.body)['cast'] as List;
      return movies.map((movie) => CreditDAO.fromJSON(movie)).toList();
    } else {
      return null;
    }
  }

  Future<String> getVideo(int idMovie) async {
    final response = await http.get(
        "https://api.themoviedb.org/3/movie/${idMovie}/videos?api_key=2fff0af21d6ba317bfd7f575d89c66e6&language=en-US");

    if (response.statusCode == 200) {
      var movies = jsonDecode(response.body)['results'] as List;
      List<VideoDAO> listVideos =
          movies.map((movie) => VideoDAO.fromJSON(movie)).toList();
      return listVideos.length == 0 ? null : listVideos[0].key;
    } else {
      return null;
    }
  }
}
