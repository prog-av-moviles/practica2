import 'package:flutter/material.dart';

class Configuration {
  static Color colorHeader = Colors.green[300];
  static Color colorIcons = Colors.green[400];
}
