import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:practica2_mov2021/src/models/popularDAO.dart';
import 'package:practica2_mov2021/src/models/userDAO.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final _nombreBD = "MOVIESDB";
  static final _versionDB = 1;

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  _initDatabase() async {
    Directory carpeta = await getApplicationDocumentsDirectory();
    String rutaDB = join(carpeta.path, _nombreBD);
    return await openDatabase(rutaDB, version: _versionDB, onCreate: _scriptDB);
  }

  _scriptDB(Database db, int version) async {
    await db.execute(
        "CREATE TABLE tbl_perfil(id INTEGER PRIMARY KEY, nombre VARCHAR(40), email VARCHAR(40), telefono CHAR(10), foto VARCHAR(200));");
    await db.execute(
        "CREATE TABLE tbl_favorites( idFav INTEGER PRIMARY KEY,  poster_path varchar(100), id integer(11), backdrop_path varchar(100), title varchar(100), vote_average decimal(52), overview varchar(100))");
  }

  Future<int> insert(String table, Map<String, dynamic> values) async {
    var conexion = await database;
    return await conexion.insert(table, values);
  }

  Future<int> update(String table, Map<String, dynamic> values) async {
    var conexion = await database;
    return await conexion
        .update(table, values, where: 'id = ?', whereArgs: [values['id']]);
  }

  Future<int> delete(String table, int id) async {
    var conexion = await database;
    return await conexion.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  Future<UserDAO> getUser(String email) async {
    var conexion = await database;
    print(email);

    var result = await conexion
        .query('tbl_perfil', where: 'email = ?', whereArgs: [email]);

    var lista = (result).map((user) => UserDAO.fromJSON(user)).toList();
    return lista.length > 0 ? lista[0] : null;
  }

  Future<List<PopularDAO>> getFavorites() async {
    var conexion = await database;
    var result = await conexion.query('tbl_favorites');
    var lista = (result).map((item) => PopularDAO.fromJSON(item)).toList();
    print(lista);
    return lista;
  }

  Future<bool> isFavorite(int id) async {
    var conexion = await database;
    var result =
        await conexion.query('tbl_favorites', where: 'id = ?', whereArgs: [id]);
    var lista = (result).map((item) => UserDAO.fromJSON(item)).toList();
    print(lista.length > 0 ? "Si esta en favoritos" : "No lo está");
    return lista.length > 0 ? true : false;
  }
}
