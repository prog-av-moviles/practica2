class UserDAO {
  int id;
  String nombre;
  String email;
  String telefono;
  String foto;

  UserDAO({this.id, this.nombre, this.email, this.telefono, this.foto});
  
  factory UserDAO.fromJSON(Map<String, dynamic> map){
    return UserDAO(
      id: map['id'],
      nombre: map['nombre'],
      email: map['email'],
      telefono: map['telefono'],
      foto: map['foto']
    );
  }

//Regresa un mapa
  Map<String, dynamic> toJSON(){
    return{
      "id": id,
      "nombre": nombre,
      "email": email,
      "telefono": telefono,
      "foto": foto
    };
  }
}